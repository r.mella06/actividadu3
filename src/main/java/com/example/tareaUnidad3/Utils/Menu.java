package com.example.tareaUnidad3.Utils;

import com.example.tareaUnidad3.Controllers.UfController;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Menu {

    private UfController uf = new UfController();
    public void desplegarMenu(){
        System.out.println("SE NECESITA CAMBIAR LA DIRECCION DEL CSV");
        System.out.println("Ver problema 1, en el navegador hacer localhost:9090/problema1");
        System.out.println("Ver problema 2, en el navegador hacer localhost:9090/problema2");
        System.out.println("Ver problema 3, en el navegador hacer localhost:9090/problema3");
        System.out.println("--------------------------------------------------------------");
        System.out.println("Puede ejecutar las Opciones del menu");
        int opc = 0;
        do {
            System.out.println("Ejecutar las Opciones del menu");
            System.out.println("[1] - Problema 1");
            System.out.println("[2] - Problema 2");
            System.out.println("[3] - Problema 3");
            System.out.println("[4] - Finalizar");
            Scanner teclado = new Scanner(System.in);
            opc = teclado.nextInt();
            switch (opc){
                case 1:
                    uf.problema1();
                    break;
                case 2:
                    uf.problema2();
                    break;
                case 3:
                    uf.problema3();
                    break;
                case 4:
                    System.out.println("Finalizando proceso...");
                    break;
            }
        }while (opc != 4);

    }
}
