package com.example.tareaUnidad3;

import com.example.tareaUnidad3.Utils.Menu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareaUnidad3Application {

	public static void main(String[] args) {

		SpringApplication.run(TareaUnidad3Application.class, args);
		Menu menu = new Menu();
		menu.desplegarMenu();
	}


}
