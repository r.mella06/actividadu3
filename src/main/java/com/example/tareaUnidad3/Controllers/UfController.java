package com.example.tareaUnidad3.Controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Date;


@RestController
@RequestMapping("/")
public class UfController {

    @GetMapping("problema1")
    public void problema1(){

        System.out.println("PROBLEMA 1");
        resolucionProblema1();
    }

    @GetMapping("problema2")
    public void problema2(){
        System.out.println("PROBLEMA 2");
        System.out.println("El promedio de las uf de todos los meses del año 2010:");
        String mesEneroInicio= "2010-01-01";
        String mesEneroTermino= "2010-01-31";
        float promedioEnero = resolucionProblema2(mesEneroInicio,mesEneroTermino);
        System.out.println("En enero es: $"+promedioEnero );

        String mesFebreroInicio= "2010-02-01";
        String mesFebreroTermino= "2010-02-31";
        float promedioFebrero = resolucionProblema2(mesFebreroInicio,mesFebreroTermino);
        System.out.println("En Febrero es: $"+promedioFebrero );

        String mesMarzoInicio= "2010-03-01";
        String mesMarzoTermino= "2010-03-31";
        float promedioMarzo = resolucionProblema2(mesMarzoInicio,mesMarzoTermino);
        System.out.println("En Marzo es: $"+promedioMarzo );

        String mesAbrilInicio= "2010-04-01";
        String mesAbrilTermino= "2010-04-31";
        float promedioAbril = resolucionProblema2(mesAbrilInicio,mesAbrilTermino);
        System.out.println("En Abril es: $"+promedioAbril );

        String mesMayoInicio= "2010-05-01";
        String mesMayoTermino= "2010-05-31";
        float promedioMayo = resolucionProblema2(mesMayoInicio,mesMayoTermino);
        System.out.println("En Mayo es: $"+promedioMayo );

        String mesJunioInicio= "2010-06-01";
        String mesJunioTermino= "2010-06-31";
        float promedioJunio = resolucionProblema2(mesJunioInicio,mesJunioTermino);
        System.out.println("En Junio es: $"+promedioJunio );

        String mesJulioInicio= "2010-07-01";
        String mesJulioTermino= "2010-07-31";
        float promedioJulio = resolucionProblema2(mesJulioInicio,mesJulioTermino);
        System.out.println("En Julio es: $"+promedioJulio );

        String mesAgostoInicio= "2010-08-01";
        String mesAgostoTermino= "2010-08-31";
        float promedioAgosto = resolucionProblema2(mesAgostoInicio,mesAgostoTermino);
        System.out.println("En Agosto es: $"+promedioAgosto );

        String mesSeptiembreInicio= "2010-09-01";
        String mesSeptiembreTermino= "2010-09-31";
        float promedioSeptiembre = resolucionProblema2(mesSeptiembreInicio,mesSeptiembreTermino);
        System.out.println("En Septiembre es: $"+promedioSeptiembre );

        String mesOctubreInicio= "2010-10-01";
        String mesOctubreTermino= "2010-10-31";
        float promedioOctubre = resolucionProblema2(mesOctubreInicio,mesOctubreTermino);
        System.out.println("En Octubre es: $"+promedioOctubre );

        String mesNoviembreInicio= "2010-11-01";
        String mesNoviembreTermino= "2010-11-31";
        float promedioNoviembre = resolucionProblema2(mesNoviembreInicio,mesNoviembreTermino);
        System.out.println("En Noviembre es: $"+promedioNoviembre );

        String mesDiciembreInicio= "2010-12-01";
        String mesDiciembreTermino= "2010-12-31";
        float promedioDiciembre = resolucionProblema2(mesDiciembreInicio,mesDiciembreTermino);
        System.out.println("En Diciembre es: $"+promedioDiciembre );

    }

    @GetMapping("problema3")
    public void problema3(){
        System.out.println("PROBLEMA 3");
        resolucionProblema3();
    }

    public void resolucionProblema1() {

        try{
            BufferedReader rd = new BufferedReader(new FileReader("C:/Users/nacho/OneDrive/Escritorio/tareaUnidad3/src/main/java/com/example/tareaUnidad3/Data/uf.csv"));
            int valor = 0;

            String fechaI1 = "1999-07-31";
            String fechaF2 = "1999-09-01";

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            Date fecha1 = formato.parse(fechaI1);
            Date fecha2 = formato.parse(fechaF2);
            float sumaTotalUf= 0;
            float cantidadUfTotal = 0;
            float promedioUf = 0;

            String line = "";

            while ((line = rd.readLine()) != null ){
                String[] parts = line.split(",");

                if (valor != 0) {

                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
                    Date fecha = format.parse(parts[0]);

                    if(fecha.after(fecha1) && fecha.before(fecha2)){
                        cantidadUfTotal++;
                        String remplazado = ",";
                        String remplazador = "";
                        char charToreplace = remplazado.charAt(0);
                        String output = "";

                        String uf = parts[1] +","+ parts[2];
                        String ufAct = uf.replace("\"","");

                        for (int i = 0; i < ufAct.length(); i++) {
                            if (ufAct.charAt(i) == charToreplace){
                                output += remplazador;
                                ufAct.substring(i+1,ufAct.length());
                            }else{
                                output += ufAct.charAt(i);
                            }

                        }

                        float ufNumerico = Float.parseFloat(output);


                        sumaTotalUf += ufNumerico;

                        promedioUf = sumaTotalUf/cantidadUfTotal;

                    }

                }
                valor++;
            }
            System.out.println("El promedio de la uf en el mes de Agosto del año 1999 era de: $"+promedioUf);

        }catch (Exception e){
            e.printStackTrace();
        }





    }

    public float resolucionProblema2(String fechaInicialMes, String fechTerminoMes){

        try {
            BufferedReader rd = new BufferedReader(new FileReader("C:/Users/nacho/OneDrive/Escritorio/tareaUnidad3/src/main/java/com/example/tareaUnidad3/Data/uf.csv"));
            int valor = 0;

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            Date fecha1 = formato.parse(fechaInicialMes);
            Date fecha2 = formato.parse(fechTerminoMes);
            float sumaTotalUf= 0;
            float cantidadUfTotal = 0;
            float promedioUf = 0;

            String line = "";
            while ((line = rd.readLine()) != null ){
                String[] parts = line.split(",");

                if (valor != 0) {
                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
                    Date fecha = format.parse(parts[0]);

                    if(fecha.after(fecha1) && fecha.before(fecha2) || fecha.equals(fecha1) || fecha.equals(fecha2)){
                        cantidadUfTotal++;
                        String remplazado = ",";
                        String remplazador = "";
                        char charToreplace = remplazado.charAt(0);
                        String output = "";

                        String uf = parts[1] +","+ parts[2];
                        String ufAct = uf.replace("\"","");

                        for (int i = 0; i < ufAct.length(); i++) {
                            if (ufAct.charAt(i) == charToreplace){
                                output += remplazador;
                                ufAct.substring(i+1,ufAct.length());
                            }else{
                                output += ufAct.charAt(i);
                            }

                        }
                        float ufNumerico = Float.parseFloat(output);

                        sumaTotalUf += ufNumerico;

                        promedioUf = sumaTotalUf/cantidadUfTotal;

                    }

                }
                valor++;
            }
          return promedioUf;

        }catch (Exception e){
            e.printStackTrace();
        }

        return 0;
    }

    public void resolucionProblema3(){
        try{
            BufferedReader rd = new BufferedReader(new FileReader("C:/Users/nacho/OneDrive/Escritorio/tareaUnidad3/src/main/java/com/example/tareaUnidad3/Data/uf.csv"));
            int valor = 0;

            String fechaVariacion1 = "1998-01-01";
            String fechaVariacion2 = "2000-01-01";

            SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");

            Date fecha1 = formato.parse(fechaVariacion1);
            Date fecha2 = formato.parse(fechaVariacion2);
            float variacionUf= 0;
            float ufNumericoFecha1= 0;
            float ufNumericoFecha2= 0;

            String line = "";
            while ((line = rd.readLine()) != null ){
                String[] parts = line.split(",");

                if (valor != 0) {

                    SimpleDateFormat format = new SimpleDateFormat("MM/dd/yy");
                    Date fecha = format.parse(parts[0]);

                    if(fecha.equals(fecha1) || fecha.equals(fecha2)){
                        String remplazado = ",";
                        String remplazador = "";
                        char charToreplace = remplazado.charAt(0);
                        String output = "";

                        if (fecha.equals(fecha1)){
                            String uf = parts[1] +","+ parts[2];
                            String ufAct = uf.replace("\"","");

                            for (int i = 0; i < ufAct.length(); i++) {
                                if (ufAct.charAt(i) == charToreplace){
                                    output += remplazador;
                                    ufAct.substring(i+1,ufAct.length());
                                }else{
                                    output += ufAct.charAt(i);
                                }

                            }
                            ufNumericoFecha1 = Float.parseFloat(output);
                        } else if (fecha.equals(fecha2)){
                            String uf = parts[1] +","+ parts[2];
                            String ufAct = uf.replace("\"","");

                            for (int i = 0; i < ufAct.length(); i++) {
                                if (ufAct.charAt(i) == charToreplace){
                                    output += remplazador;
                                    ufAct.substring(i+1,ufAct.length());
                                }else{
                                    output += ufAct.charAt(i);
                                }

                            }
                            ufNumericoFecha2 = Float.parseFloat(output);
                        }

                    }

                }
                valor++;
            }
            variacionUf = (ufNumericoFecha2 - ufNumericoFecha1) / ufNumericoFecha1;

            variacionUf = variacionUf *100;

            System.out.println("La variacion respecto a la fecha de 01/01/2000 con 01/01/1998 es de un "+variacionUf+"%");

        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
